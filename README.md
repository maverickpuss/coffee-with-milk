# coffee-with-milk

tablet: coffee-with-milk

this package only includes those firmwares shipped
in `latte-dev/android_vendor_xiaomi_latte`, see the following tree:

the original commit is: [aaf390134608e033e8c5b545568e68dab1ed2bdf](https://github.com/latte-dev/android_vendor_xiaomi_latte/commit/aaf390134608e033e8c5b545568e68dab1ed2bdf)

```
./proprietary/
├── etc
│   └── firmware
│       ├── BIOSUPDATE.fv
│       ├── bt
│       │   └── BCM4356A2_001.003.015.0084.0268_ORC.hcd
│       ├── dfw_sst.fortemedia.bin
│       ├── dfw_sst.wprd.bin
│       ├── fw_sst_22a8.fortemedia.bin
│       ├── fw_sst_22a8.wprd.bin
│       ├── isp_acc_chromaproc_css21_2401.bin
│       ├── isp_acc_deghosting_v2_css21_2401.bin
│       ├── isp_acc_lumaproc_css21_2401.bin
│       ├── isp_acc_mfnr_em_css21_2401.bin
│       ├── isp_acc_multires_v2_css21_2401.bin
│       ├── isp_acc_sce_em_css21_2401.bin
│       ├── isp_acc_warping_v2_css21_2401.bin
│       ├── isp_acc_warping_v2_em_css21_2401.bin
│       ├── maxtouch.cfg
│       ├── maxtouch.fw
│       ├── shisp_2400b0_v21.bin
│       ├── shisp_2401a0_v21.bin
│       └── tfa98xx.cnt
└── vendor
    └── firmware
        ├── 00imx135-0-0x2-0x1.drvb
        ├── 00imx135-0-0x4-0.drvb
        ├── 00imx135-0-0x8-0.drvb
        ├── 00imx135-0-0x8-0x1.drvb
        ├── 00imx135-0-0x8-0x2.drvb
        ├── 00imx135-0-0x8-0x5.drvb
        ├── 00ov8830-0-0x2-0.drvb
        ├── 01s5k8aay.drvb
        ├── brcm
        │   ├── bcm94356wlpagbl.bin
        │   ├── fw_bcmdhd_4356a2_pcie.bin
        │   ├── fw_bcmdhd_4356a2_pcie_apsta.bin
        │   ├── fw_bcmdhd_4356a2_pcie_mfg.bin
        │   └── nvram_pcie_4356_a2.cal
        └── i915
            └── huc_gen8.bin
```

Pay close attention that the following files are actually symbol links, as
extracted from OTA update script

```
symlink("/system/vendor/firmware/brcm/fw_bcmdhd_4356a2_pcie.bin", "/system/etc/firmware/fw_bcmdhd.bin");
symlink("/system/vendor/firmware/brcm/fw_bcmdhd_4356a2_pcie_mfg.bin", "/system/etc/firmware/fw_bcmdhd_mfg.bin");
symlink("dfw_sst.fortemedia.bin", "/system/etc/firmware/dfw_sst.bin");
symlink("fw_sst_22a8.fortemedia.bin", "/system/etc/firmware/fw_sst_22a8.bin");
```
